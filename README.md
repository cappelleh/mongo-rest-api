
# About

An example POC project using a local mongoDB backend and single page HTML AngularJS frontend served by node.
Includes instructions on how to set this up as a hosted REST API for your projects with minimal effort

# Getting Stared

Some steps to quickly get started

## Tools Setup

You'll need some tools for this to work, these have to be installed on first use only (or not if already done).
These instructions are for installing on Mac OS X env so for other operating systems you'll have to do some research.

### Brew

Instructions from https://brew.sh/

    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

### NodeJS

Instructions from https://nodejs.org/en/download/package-manager/

    brew install node

### MongoDB

Instructions from https://treehouse.github.io/installation-guides/mac/mongo-mac.html

~~Open the Terminal app and type brew update.~~
~~After updating Homebrew brew install mongodb~~
~~After downloading Mongo, create the “db” directory. This is where the Mongo data files will live.~~
~~You can create the directory in the default location by running mkdir -p /data/db~~

Update: this changed a bit since then, now you have to manually download community edition from https://www.mongodb.com/try/download/community 
or sign up for their hosted solutions named MongoDB Atlas. Although then you probably don't need this project.

After extracting the downloaded mongodb archive you can create a database file and launch the local mongod server with

    touch ~/mongodb/data/db
    ~/mongodb/bin/mongod --dbpath ~/mongodb/data/db

From this point on your mongodb instance is running and available at mongodb://localhost:27017. The URL for this connection is defined at

    config/defaults.js

## Dependencies

Install dependencies with

    npm install


## Building

Optionally you can use grunt (defaults) to build. Building will compile jade files into proper HTML views.

    grunt

If you start the grunt watch task then all changes in the jade files will result in new html builds.

    grunt watch

Since build resources are included in the repo you can skip this step if you don't need any changes.

## Running

next execute with

    node server

Mongo also has to be running (see previous instructions)

    ~/mongodb/bin/mongod --dbpath ~/mongodb/data/db

Open a browser and navigate to http://localhost:3000 to see the result.

## Serving

Optionally you can also install ngrok to make this local setup available on the Internet.

    brew install ngrok

To prevent the default 30 min timeouts you can register at ngrok to get a token that can be configured per their instructions.

And then after running the project you can start ngrok with

    ngrok http 3000

## Using as a REST API

You can check contents in browser using this URL http://localhost:3000 but most interaction will
be using HTTP requests. These are the ones supported by this project.

* http://localhost:3000/api/YOUR_TOKEN/items                   Retrieve content as json
* http://localhost:3000/api/YOUR_TOKEN/items/create            Add a new item, use post body for content of item to be created
* http://localhost:3000/api/YOUR_TOKEN/items/:itemId/delete    Remove item with given itemId
* http://localhost:3000/api/YOUR_TOKEN/items/:itemId/update    Update content of item with given itemId
* http://localhost:3000/api/YOUR_TOKEN/items/:itemId/detail    Show a single item

If you serve this remove public content and change default token!

# Development

## App structure

* src directory contains source files to be compiled
* public folder contains angularjs frontend, all this content is served as public!
* config, routes and server.js contains mongo backend connection and route information

## Backend

Since we use monk there is no schema for items to be stored. This way we can just update the form in the jade files and
the new objects will be stored. Note that this add ease for POC reasons BUT also introduces security risks.

## Next Steps

* Create a script for scaffolding
* add security of some kind, checking on a single token can be sufficient for this
* improve config and read config for app
* ...

## Version History

### 0.2

* documentation update
* add URL for getting a single item by ID
* added simple token check for API requests

### 0.1

* initial working project

## References

Started as a [bumm app](https://github.com/saintedlama/bumm) but simplified afterwards to bare minimum to speed up development.
Acting on [the native mongodb driver](https://www.npmjs.org/package/mongodb) directly using instructions from
[http://mongodb.github.io/node-mongodb-native/contents.html](http://mongodb.github.io/node-mongodb-native/contents.html)
turned out a bit too much work still so I eventually used the [monk project](https://github.com/LearnBoost/monk).

* [Working with promises in AngularJS](http://markdalgleish.com/2013/06/using-promises-in-angularjs-views/)
* [Promise for resolving $http](http://stackoverflow.com/questions/12505760/angularjs-processing-http-response-in-service)
* [form validation with angularJS](http://www.ng-newsletter.com/posts/validations.html)
* [Angular ui bootstrap integration](http://angular-ui.github.io/bootstrap/)
* ...

About CORS and chrome on localhost:

* http://stackoverflow.com/questions/10883211/deadly-cors-when-http-localhost-is-the-origin
* http://williamjohnbert.com/2013/06/allow-cors-with-localhost-in-chrome/
* info on running very basic server http://stackoverflow.com/questions/6084360/node-js-as-a-simple-web-server

To update the localhost name reference add it in /etc/hosts

    127.0.0.1   new.local.name

For instructions on opening chrome on OS X with arguments check
[this superuser thread](http://superuser.com/questions/157484/start-google-chrome-on-mac-with-command-line-switches).
This will open Chromium, if you have Google Chrome replace that (similar for Canary build).

    open -a "Chromium" --args --disable-web-security

Info on getting this running on synology. The only addition here is that I had to check `enable user home service` in
`Control Panel > User & Groups > tab Advanced` at the bottom. And then the `npm install forever -g` would required `sudo`
and the resulting path would be `/usr/local/lib/node_modules/forever/bin/forever`

https://github.com/StephanThierry/nodejs4synologynas
