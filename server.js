var express = require('express');
var http = require('http');
var path = require('path');

var config = require('./config'); // our app configuration

var app = express();

// Express settings
app.disable('x-powered-by');

// Configuration
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.cookieParser(config.secret));

app.use(express.session());

app.use(app.router);
app.use(express.static(path.join(__dirname, 'public'))); // this ensures all files in public folder are served

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

require('./routes')(app); // route definitions

// Start server if not invoked by require('./app')
if (require.main === module) {
    http.createServer(app).listen(config.port, config.address, function() {
        console.log("Express server listening on %s:%d in %s mode", config.address, config.port, app.settings.env);
    });    
} else {
    // Export app if invoked by require('./app')
    module.exports = app;
}
