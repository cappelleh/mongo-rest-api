angular.module('components', [])

    .factory('alertService', function($rootScope){

        // TODO do we really need rootscope here??

        // all alerts known by this service
        $rootScope.alerts = [];

        var addAlert = function(alert){
            $rootScope.alerts.push(alert);
        }

        // close option for alerts
        var closeAlert = function(index) {
            $rootScope.alerts.splice(index, 1);
        }

        // dismiss all alerts at once
        var dismissAllAlerts = function(){
            $rootScope.alerts = [];
        }

        return{
            addAlert: addAlert,
            closeAlert: closeAlert,
            dismissAllAlerts: dismissAllAlerts
        }
    })

    .factory('backendService', function($http) {

        // TODO get config loaded here for url
       
        var getItems = function(){
            return $http.get('http://localhost:3000/api/VSQu6LvYgeargUMZ/items').then(function(response){
                return response.data;
            })
        }
        var createItem = function(data){
            return $http.post('http://localhost:3000/api/VSQu6LvYgeargUMZ/items/create', data).then(function(response){
                return response.data;
            })
        }
        var deleteItem = function(id){
            return $http.post('http://localhost:3000/VSQu6LvYgeargUMZ/api/items/'+id+'/delete').then(function(response){
                return response.data;
            })
        }
        var updateItem = function(item){
            return $http.post('http://localhost:3000/VSQu6LvYgeargUMZ/api/items/'+item._id+'/update', item).then(function(response){
                return response.data;
            })
        }
        
        return {
            getItems: getItems,
            createItem: createItem,
            deleteItem: deleteItem,
            updateItem: updateItem
        };
    })