angular.module('app', ['ui.bootstrap', 'components'])

    .controller('AlertCtrl', function($scope, alertService){

        // code for alerts, this has moved to rootscope in alertService...
        // $scope.alerts = [];

        // close option for alerts
        $scope.closeAlert = function(index) {
            alertService.closeAlert(index)
        }

        // dismiss all alerts at once
        $scope.dismissAllAlerts = function(){
            alertService.dismissAllAlerts()
        }

    })

    .controller('ItemCtrl', function($scope, alertService, backendService){

        // get all items from backend
        backendService.getItems().then(function(data){
            $scope.items = data;
        }, function(reason){
            alertService.addAlert({type:'error', msg: 'failed with reason: '+JSON.stringify(reason)});
        })

        // create new item
        $scope.createItem = function(valid){
            // cancel submit if not valid
            if(!valid)
                return;
            // a flag to check if the form is in edit state
            if( $scope.editItem ){
                backendService.updateItem($scope.item).then(function(data){
                    // inform user
                    alertService.addAlert({type:'info', msg: 'item changed'});
                    // reset form
                    $scope.item = {}
                    $scope.editItem = false;
                    // refresh data
                    backendService.getItems().then(function(data){
                        $scope.items = data
                    })
                }, function(reason){
                    alertService.addAlert({type:'error', msg: 'failed with reason: '+JSON.stringify(reason)});
                })
            }
            // otherwise we are creating a new item
            else {
                backendService.createItem($scope.item).then(function(data){
                    // inform user
                    alertService.addAlert({type:'info', msg: 'item created'});
                    // reset form
                    $scope.item = {}
                    // refresh data
                    backendService.getItems().then(function(data){
                        $scope.items = data
                    })
                }, function(reason){
                    alertService.addAlert({type:'error', msg: 'failed with reason: '+JSON.stringify(reason)});
                })
            }
        }

        // delete an item by id
        $scope.deleteItem = function(id){
            backendService.deleteItem(id).then(function(data){
                // we could in fact just remove this one element instead of fetching again...
                backendService.getItems().then(function(data){
                    $scope.items = data;
                })
            }, function(reason){
                    alertService.addAlert({type:'warning', msg: 'failed with reason: '+JSON.stringify(reason)})
            })
        }

        $scope.prepareEdit = function(item){
            $scope.item = item;
            $scope.editItem = true;
        }

        $scope.cancelEdit = function(){
            $scope.item = {};
            $scope.editItem = false;
        }

    })