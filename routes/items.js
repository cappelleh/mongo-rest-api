
var config = require('../config');

module.exports = function(app) {

    // use monk for db connection pooling
    // see https://github.com/LearnBoost/monk
    var db = require('monk')(config.db.url)
        , items = db.get('items')

    // list all available items
    app.get('/api/:token/items', function(req, res) {
        if( !(req.params.token == config.token) ){
            res.status(401).send({ error: 'Something failed!' });
            return;
        }
        // show as json
        items.find({}, function(err, items){
            if( err ) throw err;
            res.send(items);
        });
    });

    // retrieve a single item
    app.get('/api/:token/items/:itemId/detail', function(req, res) {
        if( !(req.params.token == config.token) ){
            res.status(401).send({ error: 'Something failed!' });
            return;
        }
        // show as json
        items.find({ _id : req.params.itemId }, function(err, items){
            if( err ) throw err;
            res.send(items);
        });
    });

    // insert a new item
    app.post('/api/:token/items/create', function(req, res){
        if( !(req.params.token == config.token) ){
            res.status(401).send({ error: 'Something failed!' });
            return;
        }
        var item = req.body;
        items.insert(item, function(err, docs) {
            if( err ) throw err;
            // on suc6 we should return some response that can be checked on the client side
            res.send();
        })
    })

    // delete a single item
    app.post('/api/:token/items/:itemId/delete', function(req, res) {
        if( !(req.params.token == config.token) ){
            res.status(401).send({ error: 'Something failed!' });
            return;
        }
        items.remove({ _id : req.params.itemId }, function(err) {
            if( err ) throw err;
            res.send();
        });
    });

    // delete a single item
    app.post('/api/:token/items/:itemId/update', function(req, res) {
        if( !(req.params.token == config.token) ){
            res.status(401).send({ error: 'Something failed!' });
            return;
        }
        var item = req.body;
        items.update({ _id : req.params.itemId }, item, function(err) {
            if( err ) throw err;
            res.send();
        });
    });

}
